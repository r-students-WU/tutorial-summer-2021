# Repository for the VW-SozÖk-Seep R-Tutorial Summer 2021

## Dates & Location

Time: always (tbd)  17:00 - 19:00 (5pm - 7pm)

* Wednesday, April 14
* Wednesday, April 21
* Wednesday, April 28
* Wednesday, May 5

<!--

* Monday, October 19
* Monday, November 2
* Monday, November 9
* Monday, November 16

-->

via MS Teams: https://teams.microsoft.com/l/channel/19%3a5d49890023374cc8b12cb07a47eb1545%40thread.tacv2/Allgemein?groupId=3e2c0df5-c3cd-4d06-8305-b5e239ba9d85&tenantId=0504f721-d451-402b-b884-381428559e39

## Software downloads

***You don't have to install anything beforehands - We'll set up R at the beginning of the first session***

### R

- Debian/Ubuntu: `sudo apt install r-base`
    - also see `https://gitlab.com/r-students-WU/DotfilesAndOtherNonRcode/tree/master/ubuntu18_04`

- RHEL/CentOS/Fedora: `sudo yum install R`

- (open)SUSE: https://cran.r-project.org/bin/linux/suse/

- MacOS: https://cran.r-project.org/bin/macosx/

- Windows: https://cran.r-project.org/bin/windows/

### R-Studio

https://www.rstudio.com/products/rstudio/download/#download

### Git

https://git-scm.com/downloads

### Other Resources

https://imsmwu.github.io/MRDA2018/_book/index.html

